<!DOCTYPE html>
<html>
<head>
	<style>
		.chat-area {
				height: 200px;
				overflow-y: scroll;
			}

		.user-query {

			border: 1px solid green;
			width: 30%;
			padding: 3px;

			}

		.user-answer {
			text-align: end;
			width: 30%;
		}
	</style>
</head>
<body>
<div class="chat-area">

</div>
<input type=text"" rows="4" cols="50" id="txtMessage" />
<input type="button" id="btnSend" value="send"/>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  crossorigin="anonymous"></script>
<script>
	$( document ).ready(function() {
		$('#txtMessage').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				checkQuestion($(this).val());
			}
		});

		$('#btnSend').click(function(){

			checkQuestion($('#txtMessage').val());
		});
	});

	function checkQuestion(question){

		//Append User Query
		htmlToApend = "<div class='user-query'>"+$('#txtMessage').val()+"</div>";
		$('.chat-area').append(htmlToApend);
		$('#txtMessage').val('');

		
		var data = { "question" : question }
		$.ajax({
                url: "chatbot.php",
                type: "post",
                data: data,
                success: function(result) {

					try {
						var obj = jQuery.parseJSON(result);
						htmlToApend = "<div class='user-answer'>"+obj.answer+"</div>";
					}
					catch(err) {
						console.log(err);
						htmlToApend = "<div class='user-answer'>Sorry Something went Wrong :( </div>"
					}
					
                    $('.chat-area').append(htmlToApend);
					var d = $('.chat-area');
					d.scrollTop(d.prop(100));
                }
            });
	}
	
	
</script>
</body>
</html>