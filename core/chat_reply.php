<?php
date_default_timezone_set('Asia/Kolkata');
include_once('connection.php');

class Reply{
	
	private $objConnection;
	private $mysqli		= null;
	private $question 	= null;
	
	public function  __construct($question) {

		$this->objConnection = new Connection();
		$this->question = $question;
    }
	
	public function check_answer(){

		//Checks Quesstion and compares it with available keywords, 
		$data = $this->question_lookup();

		//Handle Single Characters
		if($data['single_char']){

			return $this->give_reply($this->handle_single_char());
			
		}
		

		$query = "SELECT * FROM `chat_questions` Where LOWER(question) LIKE ? and type = ? ";
		//$query = "SELECT * FROM `chat_questions` Where LOWER(question) LIKE ? ";

		if($data['category_id'] != 0 ){
			//$query 		.= " and category_id = ? ";
		}
		

		$this->mysqli = $this->objConnection->connect();
		$strLowerQuestion = strtolower($this->question);
		$param1 = "%{$strLowerQuestion}%";
		$param2 = $data['type'];
		//$stmt = $this->mysqli->prepare("SELECT * FROM `chat_questions` WHERE question LIKE ?");
		$stmt = $this->mysqli->prepare($query);
		
		$stmt->bind_param('ss', $param1,$param2);
		
		if($data['category_id'] == 0 ){
			//$stmt->bind_param('ss', $param1,$param2);
		}else{
			//$stmt->bind_param('sss', $param1,$param2,$data['category_id']);
		}

		$stmt->execute();
		
		//print_r($stmt);
		$result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
		$stmt->close();
		
		//If question Found in db
		if(sizeof($result) > 0) {
			//if Question is not answered
			if($result[0]['answer'] == ''){
				
				if($data['category_id'] != 0 && $result[0]['category_id'] == 0){

					$this->update_category_id($result[0]['id'], $data['category_id']);
				}

				return $this->give_reply($data['probable_answer']);
				
			}else{
				return $this->give_reply($result[0]['answer']);
				
			}

		}else{
			//Question Not Found in DB insert it.
			$this->add_new_question($data);			
			return $this->give_reply($data['probable_answer']);
			
		}
	}

	/*
		
	*/
	public function add_new_question($data){
		if(!$this->mysqli){
			$this->mysqli = $this->objConnection->connect();
		}

		$stmt = $this->mysqli->prepare("INSERT INTO chat_questions (question, type,category_id) VALUES (?, ?,?)");
		$stmt->bind_param("ssi",$this->question, $data['type'],$data['category_id']);
		$stmt->execute();
		$stmt->close();
	}

	public function update_category_id($id, $category_id){
		if(!$this->mysqli){
			$this->mysqli = $this->objConnection->connect();
		}

		$stmt = $this->mysqli->prepare("update chat_questions set category_id = ? where id = ");
		$stmt->bind_param("si",$category_id, $id);
		$stmt->execute();
		$stmt->close();
	}

	/*

	*/
	public function question_lookup(){
		if(!$this->mysqli){
			$this->mysqli = $this->objConnection->connect();
		}

		$question_words = explode(' ',strtolower($this->question));
		
		//$type = sizeof($question_words) === 1 ? "keyword" : "question";

		$data = [
			'match_count' 		=> 0,
			'category_id' 		=> 0,
			'single_char' 		=> 0,
			'type'				=> 'question',
			'probable_answer'	=>  $this->decode_answer_by_category("","")
		];
	
		if(sizeof($question_words) === 1 ){
			
			$data['type'] = "keyword";

			if(strlen($question_words[0]) === 1){

				$data['single_char'] = 1;
				return $data;
			}
		}
		

		$res = $this->mysqli->query("select category_id,category_name,category_keywords,probable_answer from master_category where status = 1");
		$results = $res->fetch_all(MYSQLI_ASSOC);
				
		foreach($results as $result){

			$matchcount 		= 0;
			$category_keywords 	= explode(',', strtolower($result['category_keywords']));
			$differenceCount 	= count(array_diff($question_words, $category_keywords));
			$matchcount 		= sizeof($question_words) - $differenceCount;

			if($matchcount > $data['match_count']){

				$data['match_count'] 		= $matchcount;
				$data['category_id'] 		= $result['category_id'];
				$data['category_name'] 		= $result['category_name'];
				$data['probable_answer']	= $this->decode_answer_by_category($result['category_name'],$result['probable_answer']);
			}
		}
				
		return $data;
	}

	public function decode_answer_by_category($caregory,$anserToDecode){
		$answer = "";
		switch ($caregory) {
			case "initial":
				$answer = str_replace("####",$this->wish_day(date("H")),$anserToDecode);
				break;
			default:
				$answer =  "Hey I am confused right now but suery get back to you soon with answer";
		}
		return $answer;
	}

	public function handle_single_char(){

		if(is_numeric($this->question)){
			return "it is number ".$this->question;
		}else{
			return "it is alphabate ".$this->question;
		}

		
	}

	/*
		Answer to Reply
		Stores Question in Session Log
	*/
	public function give_reply($answer){
		return json_encode(['answer' => $answer]);
	}

	/*
		
	*/
	function wish_day($time){
	
		/* This sets the $time variable to the current hour in the 24 hour clock format */
		$time = date("H");
		/* Set the $timezone variable to become the current timezone */
		$timezone = date("e");
		/* If the time is less than 1200 hours, show good morning */
		if ($time < "12") {
			return "morning";
		} else
		/* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
		if ($time >= "12" && $time < "17") {
			return "afternoon";
		} else
		/* Should the time be between or equal to 1700 and 1900 hours, show good evening */
		if ($time >= "17" && $time < "19") {
			return "evening";
		} else
		/* Finally, show good night if the time is greater than or equal to 1900 hours */
		if ($time >= "19") {
			return "night";
		}
	}
	
	public function  __destruct() {       
				
		//Close Connection
		if($this->mysqli){
			$this->objConnection->close($this->mysqli);
			
		}
		$this->objConnection = null;
    }
	
}