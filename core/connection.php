<?php

Class Connection {
	
	var $host = 'localhost';
    var $user = 'root';
    var $pass = '';
    var $db = 'chat_bot_db';
    var $mysqli;

    function connect() {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        try {
            $this->mysqli = new mysqli($this->host, $this->user, $this->pass, $this->db);
            $this->mysqli->set_charset("utf8mb4");
            return $this->mysqli;
            
          } catch(Exception $e) {
            error_log($e->getMessage());
            exit('Error connecting to database'); //Should be a message a typical user could understand
          }
    }

    function close($conn) {
        $this->mysqli->close();
        unset($this->mysqli);
    }
}
