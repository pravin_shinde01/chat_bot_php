<?php

class Chat_Manager_Session{

    public function  __construct() {
        session_start();
    }

    public function check_question($question){

        $question_manager = [];
        if($_SESSION['question_manager']){
            $question_manager = $_SESSION['question_manager'];
        }else{

        }

        $key = array_search(strtolower($question), array_column($question_manager, 'question'));

        //  echo var_dump(gettype($key));
        //  exit;

        if(gettype($key) == "integer"){
            $question_manager[$key]['frequency'] = $question_manager[$key]['frequency'] + 1;
            $_SESSION['question_manager'] = $question_manager;
            return ['question' => $question, 'frequency' => $question_manager[$key]['frequency'] + 1];
        }else{
            array_push($question_manager,["question" => $question,"frequency" => 1]);
            $_SESSION['question_manager'] = $question_manager;
            return ["question" => $question,"frequency" => 1];
        }
    }
}